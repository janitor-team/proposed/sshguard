/* A Bison parser, made by GNU Bison 3.2.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

#ifndef YY_YY_ATTACK_PARSER_H_INCLUDED
# define YY_YY_ATTACK_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IPv4 = 258,
    IPv6 = 259,
    HOSTADDR = 260,
    WORD = 261,
    INTEGER = 262,
    SYSLOG_BANNER_PID = 263,
    SOCKLOG_BANNER_PID = 264,
    BUSYBOX_SYSLOG_BANNER_PID = 265,
    SYSLOG_BANNER = 266,
    TIMESTAMP_SYSLOG = 267,
    TIMESTAMP_ISO8601 = 268,
    TIMESTAMP_TAI64 = 269,
    AT_TIMESTAMP_TAI64 = 270,
    METALOG_BANNER = 271,
    SOCKLOG_BANNER = 272,
    REPETITIONS = 273,
    HTTP_REQUEST = 274,
    HTTP_VERSION = 275,
    HTTP_REDIRECT = 276,
    HTTP_AUTHFAIL = 277,
    HTTP_CLIERROR = 278,
    HTTP_BOTSEARCH_WEBMAIL = 279,
    HTTP_BOTSEARCH_PHPMYADMIN = 280,
    HTTP_BOTSEARCH_WORDPRESS = 281,
    HTTP_BOTSEARCH = 282,
    SSH_INVALUSERPREF = 283,
    SSH_NOTALLOWEDPREF = 284,
    SSH_NOTALLOWEDSUFF = 285,
    SSH_LOGINERR_PREF = 286,
    SSH_LOGINERR_PAM = 287,
    SSH_VIA = 288,
    SSH_MAXAUTH = 289,
    SSH_ADDR_SUFF = 290,
    SSH_NOIDENTIFSTR = 291,
    SSH_BADPROTOCOLIDENTIF = 292,
    SSH_BADPROTOCOLIDENTIF_SUFF = 293,
    SSH_BADKEX_PREF = 294,
    SSH_BADKEX_SUFF = 295,
    SSH_DISCONNECT_PREF = 296,
    SSH_CONNECTION_CLOSED = 297,
    SSH_PREAUTH_SUFF = 298,
    SSHGUARD_ATTACK_PREF = 299,
    SSHGUARD_ATTACK_SUFF = 300,
    SSHGUARD_BLOCK_PREF = 301,
    SSHGUARD_BLOCK_SUFF = 302,
    DOVECOT_IMAP_LOGINERR_PREF = 303,
    DOVECOT_IMAP_LOGINERR_SUFF = 304,
    UWIMAP_LOGINERR = 305,
    CYRUSIMAP_SASL_LOGINERR_PREF = 306,
    CYRUSIMAP_SASL_LOGINERR_SUFF = 307,
    CYRUSIMAP_TLS_ERR_PREF = 308,
    CUCIPOP_AUTHFAIL = 309,
    EXIM_ESMTP_AUTHFAIL_PREF = 310,
    EXIM_ESMTP_AUTHFAIL_SUFF = 311,
    EXIM_ESMTP_LOGINFAIL_PREF = 312,
    EXIM_ESMTP_LOGINFAIL_SUFF = 313,
    SENDMAIL_RELAYDENIED_PREF = 314,
    SENDMAIL_RELAYDENIED_SUFF = 315,
    SENDMAIL_AUTHFAILURE_PREF = 316,
    SENDMAIL_AUTHFAILURE_SUFF = 317,
    POSTFIX_NO_AUTH_PREF = 318,
    POSTFIX_SASL_LOGINERR_PREF = 319,
    POSTFIX_SASL_LOGINERR_SUFF = 320,
    POSTFIX_GREYLIST = 321,
    POSTFIX_GREYLIST_SUFF = 322,
    FREEBSDFTPD_LOGINERR_PREF = 323,
    FREEBSDFTPD_LOGINERR_SUFF = 324,
    PROFTPD_LOGINERR_PREF = 325,
    PROFTPD_LOGINERR_SUFF = 326,
    PUREFTPD_LOGINERR_PREF = 327,
    PUREFTPD_LOGINERR_SUFF = 328,
    VSFTPD_LOGINERR_PREF = 329,
    VSFTPD_LOGINERR_SUFF = 330,
    COCKPIT_AUTHFAIL_PREF = 331,
    COCKPIT_AUTHFAIL_SUFF = 332,
    CLF_REQUEST_PREF = 333,
    CLF_UNAUTHOIRIZED_PREF = 334,
    CLF_UNAUTHOIRIZED_SUFF = 335,
    CLFWEBPROBES_BOTSEARCH_SUFF = 336,
    CLF_WORDPRESS_SUFF = 337,
    OPENSMTPD_FAILED_CMD_PREF = 338,
    OPENSMTPD_AUTHFAIL_SUFF = 339,
    OPENSMTPD_UNSUPPORTED_CMD_SUFF = 340,
    COURIER_AUTHFAIL_PREF = 341,
    OPENVPN_TLS_ERR_SUFF = 342
  };
#endif
/* Tokens.  */
#define IPv4 258
#define IPv6 259
#define HOSTADDR 260
#define WORD 261
#define INTEGER 262
#define SYSLOG_BANNER_PID 263
#define SOCKLOG_BANNER_PID 264
#define BUSYBOX_SYSLOG_BANNER_PID 265
#define SYSLOG_BANNER 266
#define TIMESTAMP_SYSLOG 267
#define TIMESTAMP_ISO8601 268
#define TIMESTAMP_TAI64 269
#define AT_TIMESTAMP_TAI64 270
#define METALOG_BANNER 271
#define SOCKLOG_BANNER 272
#define REPETITIONS 273
#define HTTP_REQUEST 274
#define HTTP_VERSION 275
#define HTTP_REDIRECT 276
#define HTTP_AUTHFAIL 277
#define HTTP_CLIERROR 278
#define HTTP_BOTSEARCH_WEBMAIL 279
#define HTTP_BOTSEARCH_PHPMYADMIN 280
#define HTTP_BOTSEARCH_WORDPRESS 281
#define HTTP_BOTSEARCH 282
#define SSH_INVALUSERPREF 283
#define SSH_NOTALLOWEDPREF 284
#define SSH_NOTALLOWEDSUFF 285
#define SSH_LOGINERR_PREF 286
#define SSH_LOGINERR_PAM 287
#define SSH_VIA 288
#define SSH_MAXAUTH 289
#define SSH_ADDR_SUFF 290
#define SSH_NOIDENTIFSTR 291
#define SSH_BADPROTOCOLIDENTIF 292
#define SSH_BADPROTOCOLIDENTIF_SUFF 293
#define SSH_BADKEX_PREF 294
#define SSH_BADKEX_SUFF 295
#define SSH_DISCONNECT_PREF 296
#define SSH_CONNECTION_CLOSED 297
#define SSH_PREAUTH_SUFF 298
#define SSHGUARD_ATTACK_PREF 299
#define SSHGUARD_ATTACK_SUFF 300
#define SSHGUARD_BLOCK_PREF 301
#define SSHGUARD_BLOCK_SUFF 302
#define DOVECOT_IMAP_LOGINERR_PREF 303
#define DOVECOT_IMAP_LOGINERR_SUFF 304
#define UWIMAP_LOGINERR 305
#define CYRUSIMAP_SASL_LOGINERR_PREF 306
#define CYRUSIMAP_SASL_LOGINERR_SUFF 307
#define CYRUSIMAP_TLS_ERR_PREF 308
#define CUCIPOP_AUTHFAIL 309
#define EXIM_ESMTP_AUTHFAIL_PREF 310
#define EXIM_ESMTP_AUTHFAIL_SUFF 311
#define EXIM_ESMTP_LOGINFAIL_PREF 312
#define EXIM_ESMTP_LOGINFAIL_SUFF 313
#define SENDMAIL_RELAYDENIED_PREF 314
#define SENDMAIL_RELAYDENIED_SUFF 315
#define SENDMAIL_AUTHFAILURE_PREF 316
#define SENDMAIL_AUTHFAILURE_SUFF 317
#define POSTFIX_NO_AUTH_PREF 318
#define POSTFIX_SASL_LOGINERR_PREF 319
#define POSTFIX_SASL_LOGINERR_SUFF 320
#define POSTFIX_GREYLIST 321
#define POSTFIX_GREYLIST_SUFF 322
#define FREEBSDFTPD_LOGINERR_PREF 323
#define FREEBSDFTPD_LOGINERR_SUFF 324
#define PROFTPD_LOGINERR_PREF 325
#define PROFTPD_LOGINERR_SUFF 326
#define PUREFTPD_LOGINERR_PREF 327
#define PUREFTPD_LOGINERR_SUFF 328
#define VSFTPD_LOGINERR_PREF 329
#define VSFTPD_LOGINERR_SUFF 330
#define COCKPIT_AUTHFAIL_PREF 331
#define COCKPIT_AUTHFAIL_SUFF 332
#define CLF_REQUEST_PREF 333
#define CLF_UNAUTHOIRIZED_PREF 334
#define CLF_UNAUTHOIRIZED_SUFF 335
#define CLFWEBPROBES_BOTSEARCH_SUFF 336
#define CLF_WORDPRESS_SUFF 337
#define OPENSMTPD_FAILED_CMD_PREF 338
#define OPENSMTPD_AUTHFAIL_SUFF 339
#define OPENSMTPD_UNSUPPORTED_CMD_SUFF 340
#define COURIER_AUTHFAIL_PREF 341
#define OPENVPN_TLS_ERR_SUFF 342

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 43 "attack_parser.y" /* yacc.c:1906  */

    char *str;
    int num;

#line 236 "attack_parser.h" /* yacc.c:1906  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (attack_t *attack);

#endif /* !YY_YY_ATTACK_PARSER_H_INCLUDED  */
