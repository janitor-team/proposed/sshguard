/* A Bison parser, made by GNU Bison 3.2.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.2.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "attack_parser.y" /* yacc.c:338  */


/*
 * Copyright (c) 2007,2008,2009,2010 Mij <mij@sshguard.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * SSHGuard. See http://www.sshguard.net
 */

#include <string.h>

#include "parser.h"

#define DEFAULT_ATTACKS_DANGEROUSNESS           10

 /* stuff exported by the scanner */
extern void scanner_init();
extern void scanner_fin();
extern int yylex();

static void yyerror(attack_t *, const char *);


#line 106 "attack_parser.c" /* yacc.c:338  */
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_ATTACK_PARSER_H_INCLUDED
# define YY_YY_ATTACK_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IPv4 = 258,
    IPv6 = 259,
    HOSTADDR = 260,
    WORD = 261,
    INTEGER = 262,
    SYSLOG_BANNER_PID = 263,
    SOCKLOG_BANNER_PID = 264,
    BUSYBOX_SYSLOG_BANNER_PID = 265,
    SYSLOG_BANNER = 266,
    TIMESTAMP_SYSLOG = 267,
    TIMESTAMP_ISO8601 = 268,
    TIMESTAMP_TAI64 = 269,
    AT_TIMESTAMP_TAI64 = 270,
    METALOG_BANNER = 271,
    SOCKLOG_BANNER = 272,
    REPETITIONS = 273,
    HTTP_REQUEST = 274,
    HTTP_VERSION = 275,
    HTTP_REDIRECT = 276,
    HTTP_AUTHFAIL = 277,
    HTTP_CLIERROR = 278,
    HTTP_BOTSEARCH_WEBMAIL = 279,
    HTTP_BOTSEARCH_PHPMYADMIN = 280,
    HTTP_BOTSEARCH_WORDPRESS = 281,
    HTTP_BOTSEARCH = 282,
    SSH_INVALUSERPREF = 283,
    SSH_NOTALLOWEDPREF = 284,
    SSH_NOTALLOWEDSUFF = 285,
    SSH_LOGINERR_PREF = 286,
    SSH_LOGINERR_PAM = 287,
    SSH_VIA = 288,
    SSH_MAXAUTH = 289,
    SSH_ADDR_SUFF = 290,
    SSH_NOIDENTIFSTR = 291,
    SSH_BADPROTOCOLIDENTIF = 292,
    SSH_BADPROTOCOLIDENTIF_SUFF = 293,
    SSH_BADKEX_PREF = 294,
    SSH_BADKEX_SUFF = 295,
    SSH_DISCONNECT_PREF = 296,
    SSH_CONNECTION_CLOSED = 297,
    SSH_PREAUTH_SUFF = 298,
    SSHGUARD_ATTACK_PREF = 299,
    SSHGUARD_ATTACK_SUFF = 300,
    SSHGUARD_BLOCK_PREF = 301,
    SSHGUARD_BLOCK_SUFF = 302,
    DOVECOT_IMAP_LOGINERR_PREF = 303,
    DOVECOT_IMAP_LOGINERR_SUFF = 304,
    UWIMAP_LOGINERR = 305,
    CYRUSIMAP_SASL_LOGINERR_PREF = 306,
    CYRUSIMAP_SASL_LOGINERR_SUFF = 307,
    CYRUSIMAP_TLS_ERR_PREF = 308,
    CUCIPOP_AUTHFAIL = 309,
    EXIM_ESMTP_AUTHFAIL_PREF = 310,
    EXIM_ESMTP_AUTHFAIL_SUFF = 311,
    EXIM_ESMTP_LOGINFAIL_PREF = 312,
    EXIM_ESMTP_LOGINFAIL_SUFF = 313,
    SENDMAIL_RELAYDENIED_PREF = 314,
    SENDMAIL_RELAYDENIED_SUFF = 315,
    SENDMAIL_AUTHFAILURE_PREF = 316,
    SENDMAIL_AUTHFAILURE_SUFF = 317,
    POSTFIX_NO_AUTH_PREF = 318,
    POSTFIX_SASL_LOGINERR_PREF = 319,
    POSTFIX_SASL_LOGINERR_SUFF = 320,
    POSTFIX_GREYLIST = 321,
    POSTFIX_GREYLIST_SUFF = 322,
    FREEBSDFTPD_LOGINERR_PREF = 323,
    FREEBSDFTPD_LOGINERR_SUFF = 324,
    PROFTPD_LOGINERR_PREF = 325,
    PROFTPD_LOGINERR_SUFF = 326,
    PUREFTPD_LOGINERR_PREF = 327,
    PUREFTPD_LOGINERR_SUFF = 328,
    VSFTPD_LOGINERR_PREF = 329,
    VSFTPD_LOGINERR_SUFF = 330,
    COCKPIT_AUTHFAIL_PREF = 331,
    COCKPIT_AUTHFAIL_SUFF = 332,
    CLF_REQUEST_PREF = 333,
    CLF_UNAUTHOIRIZED_PREF = 334,
    CLF_UNAUTHOIRIZED_SUFF = 335,
    CLFWEBPROBES_BOTSEARCH_SUFF = 336,
    CLF_WORDPRESS_SUFF = 337,
    OPENSMTPD_FAILED_CMD_PREF = 338,
    OPENSMTPD_AUTHFAIL_SUFF = 339,
    OPENSMTPD_UNSUPPORTED_CMD_SUFF = 340,
    COURIER_AUTHFAIL_PREF = 341,
    OPENVPN_TLS_ERR_SUFF = 342
  };
#endif
/* Tokens.  */
#define IPv4 258
#define IPv6 259
#define HOSTADDR 260
#define WORD 261
#define INTEGER 262
#define SYSLOG_BANNER_PID 263
#define SOCKLOG_BANNER_PID 264
#define BUSYBOX_SYSLOG_BANNER_PID 265
#define SYSLOG_BANNER 266
#define TIMESTAMP_SYSLOG 267
#define TIMESTAMP_ISO8601 268
#define TIMESTAMP_TAI64 269
#define AT_TIMESTAMP_TAI64 270
#define METALOG_BANNER 271
#define SOCKLOG_BANNER 272
#define REPETITIONS 273
#define HTTP_REQUEST 274
#define HTTP_VERSION 275
#define HTTP_REDIRECT 276
#define HTTP_AUTHFAIL 277
#define HTTP_CLIERROR 278
#define HTTP_BOTSEARCH_WEBMAIL 279
#define HTTP_BOTSEARCH_PHPMYADMIN 280
#define HTTP_BOTSEARCH_WORDPRESS 281
#define HTTP_BOTSEARCH 282
#define SSH_INVALUSERPREF 283
#define SSH_NOTALLOWEDPREF 284
#define SSH_NOTALLOWEDSUFF 285
#define SSH_LOGINERR_PREF 286
#define SSH_LOGINERR_PAM 287
#define SSH_VIA 288
#define SSH_MAXAUTH 289
#define SSH_ADDR_SUFF 290
#define SSH_NOIDENTIFSTR 291
#define SSH_BADPROTOCOLIDENTIF 292
#define SSH_BADPROTOCOLIDENTIF_SUFF 293
#define SSH_BADKEX_PREF 294
#define SSH_BADKEX_SUFF 295
#define SSH_DISCONNECT_PREF 296
#define SSH_CONNECTION_CLOSED 297
#define SSH_PREAUTH_SUFF 298
#define SSHGUARD_ATTACK_PREF 299
#define SSHGUARD_ATTACK_SUFF 300
#define SSHGUARD_BLOCK_PREF 301
#define SSHGUARD_BLOCK_SUFF 302
#define DOVECOT_IMAP_LOGINERR_PREF 303
#define DOVECOT_IMAP_LOGINERR_SUFF 304
#define UWIMAP_LOGINERR 305
#define CYRUSIMAP_SASL_LOGINERR_PREF 306
#define CYRUSIMAP_SASL_LOGINERR_SUFF 307
#define CYRUSIMAP_TLS_ERR_PREF 308
#define CUCIPOP_AUTHFAIL 309
#define EXIM_ESMTP_AUTHFAIL_PREF 310
#define EXIM_ESMTP_AUTHFAIL_SUFF 311
#define EXIM_ESMTP_LOGINFAIL_PREF 312
#define EXIM_ESMTP_LOGINFAIL_SUFF 313
#define SENDMAIL_RELAYDENIED_PREF 314
#define SENDMAIL_RELAYDENIED_SUFF 315
#define SENDMAIL_AUTHFAILURE_PREF 316
#define SENDMAIL_AUTHFAILURE_SUFF 317
#define POSTFIX_NO_AUTH_PREF 318
#define POSTFIX_SASL_LOGINERR_PREF 319
#define POSTFIX_SASL_LOGINERR_SUFF 320
#define POSTFIX_GREYLIST 321
#define POSTFIX_GREYLIST_SUFF 322
#define FREEBSDFTPD_LOGINERR_PREF 323
#define FREEBSDFTPD_LOGINERR_SUFF 324
#define PROFTPD_LOGINERR_PREF 325
#define PROFTPD_LOGINERR_SUFF 326
#define PUREFTPD_LOGINERR_PREF 327
#define PUREFTPD_LOGINERR_SUFF 328
#define VSFTPD_LOGINERR_PREF 329
#define VSFTPD_LOGINERR_SUFF 330
#define COCKPIT_AUTHFAIL_PREF 331
#define COCKPIT_AUTHFAIL_SUFF 332
#define CLF_REQUEST_PREF 333
#define CLF_UNAUTHOIRIZED_PREF 334
#define CLF_UNAUTHOIRIZED_SUFF 335
#define CLFWEBPROBES_BOTSEARCH_SUFF 336
#define CLF_WORDPRESS_SUFF 337
#define OPENSMTPD_FAILED_CMD_PREF 338
#define OPENSMTPD_AUTHFAIL_SUFF 339
#define OPENSMTPD_UNSUPPORTED_CMD_SUFF 340
#define COURIER_AUTHFAIL_PREF 341
#define OPENVPN_TLS_ERR_SUFF 342

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 43 "attack_parser.y" /* yacc.c:353  */

    char *str;
    int num;

#line 328 "attack_parser.c" /* yacc.c:353  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (attack_t *attack);

#endif /* !YY_YY_ATTACK_PARSER_H_INCLUDED  */



#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  120
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   267

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  91
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  36
/* YYNRULES -- Number of rules.  */
#define YYNRULES  89
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  167

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   342

#define YYTRANSLATE(YYX)                                                \
  ((unsigned) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,    88,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    89,     2,    90,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   116,   116,   117,   118,   119,   120,   121,   134,   137,
     138,   139,   144,   148,   153,   154,   159,   164,   167,   168,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     196,   200,   205,   211,   223,   225,   226,   227,   228,   233,
     234,   236,   240,   241,   242,   243,   247,   248,   249,   250,
     254,   258,   263,   264,   269,   274,   278,   279,   284,   289,
     290,   294,   295,   299,   300,   301,   306,   311,   316,   321,
     326,   327,   332,   337,   342,   347,   348,   353,   358,   359
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "IPv4", "IPv6", "HOSTADDR", "WORD",
  "INTEGER", "SYSLOG_BANNER_PID", "SOCKLOG_BANNER_PID",
  "BUSYBOX_SYSLOG_BANNER_PID", "SYSLOG_BANNER", "TIMESTAMP_SYSLOG",
  "TIMESTAMP_ISO8601", "TIMESTAMP_TAI64", "AT_TIMESTAMP_TAI64",
  "METALOG_BANNER", "SOCKLOG_BANNER", "REPETITIONS", "HTTP_REQUEST",
  "HTTP_VERSION", "HTTP_REDIRECT", "HTTP_AUTHFAIL", "HTTP_CLIERROR",
  "HTTP_BOTSEARCH_WEBMAIL", "HTTP_BOTSEARCH_PHPMYADMIN",
  "HTTP_BOTSEARCH_WORDPRESS", "HTTP_BOTSEARCH", "SSH_INVALUSERPREF",
  "SSH_NOTALLOWEDPREF", "SSH_NOTALLOWEDSUFF", "SSH_LOGINERR_PREF",
  "SSH_LOGINERR_PAM", "SSH_VIA", "SSH_MAXAUTH", "SSH_ADDR_SUFF",
  "SSH_NOIDENTIFSTR", "SSH_BADPROTOCOLIDENTIF",
  "SSH_BADPROTOCOLIDENTIF_SUFF", "SSH_BADKEX_PREF", "SSH_BADKEX_SUFF",
  "SSH_DISCONNECT_PREF", "SSH_CONNECTION_CLOSED", "SSH_PREAUTH_SUFF",
  "SSHGUARD_ATTACK_PREF", "SSHGUARD_ATTACK_SUFF", "SSHGUARD_BLOCK_PREF",
  "SSHGUARD_BLOCK_SUFF", "DOVECOT_IMAP_LOGINERR_PREF",
  "DOVECOT_IMAP_LOGINERR_SUFF", "UWIMAP_LOGINERR",
  "CYRUSIMAP_SASL_LOGINERR_PREF", "CYRUSIMAP_SASL_LOGINERR_SUFF",
  "CYRUSIMAP_TLS_ERR_PREF", "CUCIPOP_AUTHFAIL", "EXIM_ESMTP_AUTHFAIL_PREF",
  "EXIM_ESMTP_AUTHFAIL_SUFF", "EXIM_ESMTP_LOGINFAIL_PREF",
  "EXIM_ESMTP_LOGINFAIL_SUFF", "SENDMAIL_RELAYDENIED_PREF",
  "SENDMAIL_RELAYDENIED_SUFF", "SENDMAIL_AUTHFAILURE_PREF",
  "SENDMAIL_AUTHFAILURE_SUFF", "POSTFIX_NO_AUTH_PREF",
  "POSTFIX_SASL_LOGINERR_PREF", "POSTFIX_SASL_LOGINERR_SUFF",
  "POSTFIX_GREYLIST", "POSTFIX_GREYLIST_SUFF", "FREEBSDFTPD_LOGINERR_PREF",
  "FREEBSDFTPD_LOGINERR_SUFF", "PROFTPD_LOGINERR_PREF",
  "PROFTPD_LOGINERR_SUFF", "PUREFTPD_LOGINERR_PREF",
  "PUREFTPD_LOGINERR_SUFF", "VSFTPD_LOGINERR_PREF", "VSFTPD_LOGINERR_SUFF",
  "COCKPIT_AUTHFAIL_PREF", "COCKPIT_AUTHFAIL_SUFF", "CLF_REQUEST_PREF",
  "CLF_UNAUTHOIRIZED_PREF", "CLF_UNAUTHOIRIZED_SUFF",
  "CLFWEBPROBES_BOTSEARCH_SUFF", "CLF_WORDPRESS_SUFF",
  "OPENSMTPD_FAILED_CMD_PREF", "OPENSMTPD_AUTHFAIL_SUFF",
  "OPENSMTPD_UNSUPPORTED_CMD_SUFF", "COURIER_AUTHFAIL_PREF",
  "OPENVPN_TLS_ERR_SUFF", "'%'", "'['", "']'", "$accept", "text",
  "syslogent", "multilogent", "metalogent", "socklogent", "busyboxent",
  "logmsg", "repetition_suffix", "msg_single", "addr", "sshmsg",
  "ssh_illegaluser", "ssh_authfail", "ssh_noidentifstring",
  "ssh_badprotocol", "ssh_badkex", "sshguardmsg", "dovecotmsg",
  "uwimapmsg", "cyrusimapmsg", "cucipopmsg", "eximmsg", "sendmailmsg",
  "postfixmsg", "freebsdftpdmsg", "proftpdmsg", "pureftpdmsg", "vsftpdmsg",
  "cockpitmsg", "clfunauhtdmsg", "clfwebprobesmsg", "clfwordpressmsg",
  "opensmtpdmsg", "couriermsg", "openvpnmsg", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,    37,    91,
      93
};
# endif

#define YYPACT_NINF -76

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-76)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      29,   -76,   -75,   -76,   178,   178,   178,   178,   104,   178,
     178,   178,   116,   116,   116,   116,   116,   116,   116,   116,
     116,   116,   116,   116,   116,   -74,   116,   -59,   116,   116,
     116,   116,   116,   116,   116,   116,   116,   116,   116,   116,
     116,   116,   -48,   116,    59,   -76,   -76,   -76,   -76,   -76,
     -76,    25,   -23,   -76,   -76,   -76,   -76,   -76,   -76,   -76,
     -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,
     -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,    56,   -76,
     -76,   -76,   -76,   178,   -76,   -76,   -76,   -76,    34,    37,
      39,    45,    41,    46,    47,    54,    44,    53,    27,    51,
      55,   116,    48,   116,   -76,    60,    64,    42,    49,    33,
      52,    57,    58,    63,    66,    50,    65,   -49,   116,    38,
     -76,   -76,   -76,    32,    67,   -76,   -76,   -76,   -76,   -76,
     -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,
     -76,    40,   -76,    61,   -76,   -76,   -76,   -76,   -76,   -76,
     -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,    70,    62,
     -76,   -76,   -76,   -76,   -76,   -76,   -76
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,    40,    41,    43,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     2,     3,     4,     5,     6,
       7,    18,     0,    20,    44,    45,    46,    47,    48,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,     0,     8,
      14,    16,     9,     0,    10,    12,    13,    15,    49,     0,
       0,    53,     0,    56,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    68,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    81,     0,     0,     0,
       1,    19,    17,     0,     0,    88,    42,    11,    50,    51,
      52,    54,    55,    57,    60,    61,    58,    59,    62,    63,
      64,     0,    66,     0,    69,    70,    71,    72,    74,    73,
      75,    76,    77,    78,    79,    80,    85,    86,     0,     0,
      83,    84,    82,    65,    67,    87,    89
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -76,   -76,   -76,   -76,   -76,   -76,   -76,    43,   -76,   -76,
     -12,   -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,
     -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,   -76,
     -76,   -76,   -76,   -76,   -76,   -76
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,    44,    45,    46,    47,    48,    49,    50,   122,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,    78,   102,   101,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     103,   119,     1,     2,     3,   156,   157,     4,     5,     6,
       7,   118,     8,   121,     9,    10,    11,    79,    80,    81,
      82,    84,    85,    86,    87,   123,   124,    12,    13,   120,
      14,    15,   126,    16,   125,    17,    18,   129,    19,   128,
      20,    21,   138,    22,   130,    23,   132,    24,   131,    25,
      26,   133,    27,    28,    29,   134,    30,   136,    31,   141,
      32,   143,    33,    34,   135,    35,   137,    36,   139,    37,
     142,    38,   146,    39,   140,    40,   158,     1,     2,     3,
      83,   147,    41,   160,   161,    42,   144,   149,    43,     1,
       2,     3,   145,   148,   150,   154,   127,   151,   159,     0,
     163,     0,    12,    13,   152,    14,    15,     0,    16,   153,
      17,    18,   155,    19,     0,    20,    21,   162,    22,   166,
      23,   164,    24,     0,    25,    26,     0,    27,    28,    29,
     165,    30,     0,    31,     0,    32,     0,    33,    34,     0,
      35,     0,    36,     0,    37,     0,    38,     0,    39,     0,
      40,     1,     2,     3,     0,     0,     0,    41,     0,     0,
      42,     0,     0,    43,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    12,    13,     0,    14,
      15,     0,    16,     0,    17,    18,     0,    19,     0,    20,
      21,     0,    22,     0,    23,     0,    24,     0,    25,    26,
       0,    27,    28,    29,     0,    30,     0,    31,     0,    32,
       0,    33,    34,     0,    35,     0,    36,     0,    37,     0,
      38,     0,    39,     0,    40,     0,     0,     0,     0,     0,
       0,    41,     0,     0,    42,     0,     0,    43
};

static const yytype_int8 yycheck[] =
{
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    88,    26,    89,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      89,    43,     3,     4,     5,    84,    85,     8,     9,    10,
      11,    89,    13,    18,    15,    16,    17,     4,     5,     6,
       7,     8,     9,    10,    11,    78,    79,    28,    29,     0,
      31,    32,     6,    34,    87,    36,    37,    30,    39,    35,
      41,    42,    45,    44,    35,    46,    35,    48,    33,    50,
      51,    35,    53,    54,    55,    38,    57,    43,    59,   101,
      61,   103,    63,    64,    40,    66,    43,    68,    47,    70,
      52,    72,    60,    74,    49,    76,   118,     3,     4,     5,
       6,    62,    83,    81,    82,    86,    56,    65,    89,     3,
       4,     5,    58,    90,    67,    75,    83,    69,    90,    -1,
      90,    -1,    28,    29,    71,    31,    32,    -1,    34,    73,
      36,    37,    77,    39,    -1,    41,    42,    80,    44,    87,
      46,    90,    48,    -1,    50,    51,    -1,    53,    54,    55,
      90,    57,    -1,    59,    -1,    61,    -1,    63,    64,    -1,
      66,    -1,    68,    -1,    70,    -1,    72,    -1,    74,    -1,
      76,     3,     4,     5,    -1,    -1,    -1,    83,    -1,    -1,
      86,    -1,    -1,    89,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    28,    29,    -1,    31,
      32,    -1,    34,    -1,    36,    37,    -1,    39,    -1,    41,
      42,    -1,    44,    -1,    46,    -1,    48,    -1,    50,    51,
      -1,    53,    54,    55,    -1,    57,    -1,    59,    -1,    61,
      -1,    63,    64,    -1,    66,    -1,    68,    -1,    70,    -1,
      72,    -1,    74,    -1,    76,    -1,    -1,    -1,    -1,    -1,
      -1,    83,    -1,    -1,    86,    -1,    -1,    89
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,     5,     8,     9,    10,    11,    13,    15,
      16,    17,    28,    29,    31,    32,    34,    36,    37,    39,
      41,    42,    44,    46,    48,    50,    51,    53,    54,    55,
      57,    59,    61,    63,    64,    66,    68,    70,    72,    74,
      76,    83,    86,    89,    92,    93,    94,    95,    96,    97,
      98,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,    88,    98,
      98,    98,    98,     6,    98,    98,    98,    98,   101,   101,
     101,   101,   101,   101,   101,   101,   101,   101,   101,   101,
     101,    89,   101,    89,   101,   101,   101,   101,   101,   101,
     101,   101,   101,   101,   101,   101,   101,   101,    89,   101,
       0,    18,    99,    78,    79,    87,     6,    98,    35,    30,
      35,    33,    35,    35,    38,    40,    43,    43,    45,    47,
      49,   101,    52,   101,    56,    58,    60,    62,    90,    65,
      67,    69,    71,    73,    75,    77,    84,    85,   101,    90,
      81,    82,    80,    90,    90,    90,    87
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    91,    92,    92,    92,    92,    92,    92,    93,    93,
      93,    93,    94,    95,    96,    96,    97,    98,    99,    99,
     100,   100,   100,   100,   100,   100,   100,   100,   100,   100,
     100,   100,   100,   100,   100,   100,   100,   100,   100,   100,
     101,   101,   101,   101,   102,   102,   102,   102,   102,   103,
     103,   103,   104,   104,   104,   104,   105,   105,   105,   105,
     106,   107,   108,   108,   109,   110,   111,   111,   112,   113,
     113,   114,   114,   115,   115,   115,   116,   117,   118,   119,
     120,   120,   121,   122,   123,   124,   124,   125,   126,   126
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     1,     1,     1,     1,     2,     2,
       2,     3,     2,     2,     2,     2,     2,     2,     0,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     1,     1,     1,     1,     1,     1,     2,
       3,     3,     3,     2,     3,     3,     2,     3,     3,     3,
       3,     3,     3,     3,     3,     4,     3,     4,     2,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     2,     3,     3,     3,     3,     3,     4,     2,     4
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (attack, YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, attack); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep, attack_t *attack)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  YYUSE (attack);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep, attack_t *attack)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep, attack);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule, attack_t *attack)
{
  unsigned long yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              , attack);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule, attack); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return (YYSIZE_T) (yystpcpy (yyres, yystr) - yyres);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, attack_t *attack)
{
  YYUSE (yyvaluep);
  YYUSE (attack);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (attack_t *attack)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = (yytype_int16) yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = (YYSIZE_T) (yyssp - yyss + 1);

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 20:
#line 172 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_SSH; }
#line 1588 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 21:
#line 173 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_SSHGUARD; }
#line 1594 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 22:
#line 174 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_DOVECOT; }
#line 1600 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 23:
#line 175 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_UWIMAP; }
#line 1606 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 24:
#line 176 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_CYRUSIMAP; }
#line 1612 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 25:
#line 177 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_CUCIPOP; }
#line 1618 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 26:
#line 178 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_EXIM; }
#line 1624 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 27:
#line 179 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_SENDMAIL; }
#line 1630 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 28:
#line 180 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_POSTFIX; }
#line 1636 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 29:
#line 181 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_FREEBSDFTPD; }
#line 1642 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 30:
#line 182 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_PROFTPD; }
#line 1648 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 31:
#line 183 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_PUREFTPD; }
#line 1654 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 32:
#line 184 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_VSFTPD; }
#line 1660 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 33:
#line 185 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_COCKPIT; }
#line 1666 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 34:
#line 186 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_CLF_UNAUTH; }
#line 1672 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 35:
#line 187 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_CLF_PROBES; }
#line 1678 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 36:
#line 188 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_CLF_WORDPRESS; }
#line 1684 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 37:
#line 189 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_OPENSMTPD; }
#line 1690 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 38:
#line 190 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_COURIER; }
#line 1696 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 39:
#line 191 "attack_parser.y" /* yacc.c:1645  */
    {   attack->service = SERVICES_OPENVPN; }
#line 1702 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 40:
#line 196 "attack_parser.y" /* yacc.c:1645  */
    {
                        attack->address.kind = ADDRKIND_IPv4;
                        strcpy(attack->address.value, (yyvsp[0].str));
                    }
#line 1711 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 41:
#line 200 "attack_parser.y" /* yacc.c:1645  */
    {
                        attack->address.kind = ADDRKIND_IPv6;
                        strcpy(attack->address.value, (yyvsp[0].str));
                        free((yyvsp[0].str));
                    }
#line 1721 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 42:
#line 206 "attack_parser.y" /* yacc.c:1645  */
    {
                        attack->address.kind = ADDRKIND_IPv6;
                        strcpy(attack->address.value, (yyvsp[-2].str));
                        free((yyvsp[-2].str));
                    }
#line 1731 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 43:
#line 211 "attack_parser.y" /* yacc.c:1645  */
    {
                        if (!attack_from_hostname(attack, (yyvsp[0].str))) {
                            YYABORT;
                        }
                    }
#line 1741 "attack_parser.c" /* yacc.c:1645  */
    break;

  case 59:
#line 250 "attack_parser.y" /* yacc.c:1645  */
    { attack->dangerousness = 2; }
#line 1747 "attack_parser.c" /* yacc.c:1645  */
    break;


#line 1751 "attack_parser.c" /* yacc.c:1645  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (attack, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (attack, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, attack);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, attack);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (attack, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, attack);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, attack);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 362 "attack_parser.y" /* yacc.c:1903  */


static void yyerror(__attribute__((unused)) attack_t *a,
    __attribute__((unused)) const char *s) { /* do nothing */ }

int parse_line(char *str, attack_t *attack) {

    /* TODO: reduce danger for SERVICES_CLF_PROBES */
    attack->dangerousness = DEFAULT_ATTACKS_DANGEROUSNESS;

    scanner_init(str);
    int ret = yyparse(attack);
    scanner_fin();

    return ret;
}
